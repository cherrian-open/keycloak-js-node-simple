Secure Nodejs app with keycloak example
========================================

Example on how to secure a nodejs app using keycloak. 

This example uses
- Keycloak with postgres (keycloak version 11)
- Nodejs with express 


Build
-------

docker-compose build

Run and Configure keycloak
---------------------------

1. docker-compose up -d postgres pgadmin
    > wait for a minute for database to start
1. docker-compose up -d keycloak


Pgadmin is optional. Run $ docker-compose up -d pgadmin for database troubleshooting.


### Configure keycloak


Go to http://localhost:8080 with the username / password set in the docker-compose.yml file. (E.g. admin/password)

Add a test client called `test-client` in the master realm  
Add redirect URI as http://localhost:8000/*  
Add web origins as *  (i.e allow all. You may limit to say just http://localhost:8000)  


Add a new user say `user1` with password `password`

In this setup, keycloak uses the postgres database. 


Run Nodejs app
------------------

1. cd to node directory

    $ npm install   
    $ npm start  


> $ docker-compose up -d nodejs will not work unless a custom docker network is created in docker-compose with proxy. 
> You will receive "Access denied" if run nodejs application in docker 


Stop
----

docker-compose down


Logs
------

docker-compose logs -f


Test
-----

Go to http://localhost:8000 on web browser. 

Go to http://localhost/secure. You will be redirected to keycloak login page. Login using the user created in the keycloak. Upon login, the secure page will be displayed. 

View the console.log output (nodejs server side) to view the access_token and user_id. This can be used for delivering content specific to the user












'use strict';

const express = require('express');
const Keycloak = require('keycloak-connect');
const session = require('express-session');
const jwt_decode = require('jwt-decode');
const cors = require('cors');

// Constants
const PORT = 8000;
const HOST = 'localhost';

// App
const app = express();

var memoryStore = new session.MemoryStore();
var keycloak = new Keycloak({ store: memoryStore });

app.use(session({
  secret:'thisShouldBeLongAndSecret554',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));

app.use(keycloak.middleware());

app.use(cors());


app.get('/', (req, res) => {
  res.send('Hello World');
});

//route protected with Keycloak
app.get('/secure', keycloak.protect(), function(req, res){
  console.log('session: %s',req.session);
  console.log('keycloak-token session variable: %s',JSON.stringify(JSON.parse(req.session['keycloak-token']), null, 4));
  let keycloak_tokens = JSON.parse(req.session['keycloak-token']);
  let jwt_decoded = jwt_decode(keycloak_tokens.access_token);
  console.log('access_token %s:',keycloak_tokens.access_token);
  console.log('access_token SUB or user_id is: %s',jwt_decoded.sub);

  res.send('This is a Secure page')

});


app.use( keycloak.middleware( { 
  logout: '/logout',
  home: '/home'
} ));


app.listen(PORT, error => {
  if (error) throw error;
    console.log('node server is running on port: ' + PORT);
});